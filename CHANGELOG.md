# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.3] - 2018-09-23
### Changed
- Fixed a bug where a page that used a `Length` header instead of a
  `Content-Length` header would throw an error. If neither a `Length` nor a
  `Content-Length` is sent, the plugin will just return the `Content-Type`
  without an indication of the size of the request.

## [1.1.2] - 2018-09-13
### Changed
- Title names of URLs will now have some escaping applied to it to prevent
  people being able to use maliciously crafted URLs to make the bot perform
  commands.
